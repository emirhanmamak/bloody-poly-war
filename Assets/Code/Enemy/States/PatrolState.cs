using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolState : BaseState
{
    public int wayPointIndex;
    public float waitTimer;
    public override void Enter()
    {
    }

    public override void Perform()
    {
        PatrolCycle();
        if (enemy.CanSeeCharacter())
        {
            Debug.Log("        PatrolCycle()");
            stateMachine.ChangeState(new AttackState());
        }
    }

    public override void Exit()
    {
    }

    public void PatrolCycle()
    {
        if (enemy.Agent.remainingDistance < 0.2f)
        {
            waitTimer += Time.deltaTime;
            if(waitTimer < 3f) return;
            if (wayPointIndex < enemy.path.wayPoints.Count - 1)
            {
                wayPointIndex++;
            }
            else
            {
                wayPointIndex = 0;
            }
            enemy.Agent.SetDestination(enemy.path.wayPoints[wayPointIndex].position);
            waitTimer = 0f; 
        }
    }
}

public abstract class BaseState
{
    //Must have Instate StateMachine and Enemy Class
    public Enemy enemy;
    public StateMachine stateMachine;
    public abstract void Enter();
    public abstract void Perform();
    public abstract void Exit();
}
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackState : BaseState
{
    private float moveTimer;
    private float losePlayerTimer;
    public override void Enter()
    {
    }

    public override void Perform()
    {
        if (enemy.CanSeeCharacter())
        {
            Debug.Log("suan goruyor");
            losePlayerTimer = 0f;
            moveTimer += Time.deltaTime;
            if (moveTimer > Random.Range(3, 7))
            {
                enemy.Agent.SetDestination(enemy.transform.position + (Random.insideUnitSphere * 5f));
                moveTimer = 0;
            }
            else
            {
                losePlayerTimer += Time.deltaTime;
                if (losePlayerTimer > 3f)
                {
                    stateMachine.ChangeState(new PatrolState());
                }
            }
        }
    }

    public override void Exit()
    {
    }
}

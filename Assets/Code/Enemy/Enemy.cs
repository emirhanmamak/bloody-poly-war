using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    /*
     * #5 Game AI 8. Dakikada Bıraklıdı
     */
    
    private GameObject player;
    private StateMachine stateMachine;
    private NavMeshAgent agent;
    [SerializeField] private string currentState;
    public Path path;

    public NavMeshAgent Agent
    {
        get => agent;
    }
    
    public float canSeePlayerRange = 30f;
    public float fieldOfView = 85f;
    public float eyeHeight;

    private void Awake()
    {
        stateMachine = GetComponent<StateMachine>();
        agent = GetComponent<NavMeshAgent>();
    }


    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        stateMachine.Initialise();
    }

    private void Update()
    {
        CanSeeCharacter();
        currentState = stateMachine.activeState.ToString();
    }

    public bool CanSeeCharacter()
    {
        if (player != null)
        {
            if (canSeePlayerRange > Vector3.Distance(this.transform.position, player.transform.position))
            {
                Vector3 targetDirection = player.transform.position - transform.position - (Vector3.up * eyeHeight);
                float angleToPlayer = Vector3.Angle(targetDirection, transform.forward);
                if (angleToPlayer >= -fieldOfView && angleToPlayer <= fieldOfView)
                {
                    Ray ray = new Ray(transform.position + (Vector3.up * eyeHeight), targetDirection);
                    RaycastHit raycastHit = new RaycastHit();
                    if (Physics.Raycast(ray, out raycastHit, canSeePlayerRange))
                    {
//                      Debug.Log("canseeit1");

                        if (raycastHit.transform.gameObject == player)
                        {
                            Debug.Log("canseeit1");
                            return true;
                        }
                            Debug.DrawRay(ray.origin, ray.direction * canSeePlayerRange, Color.red);
                    }
                }
            }
        }

        return false;
    }
}
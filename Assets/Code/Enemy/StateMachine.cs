using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public BaseState activeState;

    public void Initialise()
    {
        //setup Default State
        ChangeState(new PatrolState());
    }
    public void Update()
    {
        if (activeState != null)
        {
            activeState.Perform();
        }
    }

    public void ChangeState(BaseState newState)
    {
        if (activeState != null)
        {
            //Clean State
            ClearState(activeState);
        }
        activeState = newState;
        if (activeState != null)
        {
            //Setup New State
            activeState.stateMachine = this;
            activeState.enemy = GetComponent<Enemy>();
            activeState.Enter();
        }
    }

    private void ClearState(BaseState clearState)
    {
        activeState.Exit();;
    }
}

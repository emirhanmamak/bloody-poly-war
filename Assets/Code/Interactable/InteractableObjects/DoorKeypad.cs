using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorKeypad : Interactable
{   
    [SerializeField] private GameObject door;
    private bool isDoorOpen;
    protected override void Interact()
    {
        isDoorOpen = !isDoorOpen;
        door.gameObject.GetComponent<Animator>().SetBool("isDoorOpen", isDoorOpen);
        Debug.Log("Interacted with :" + gameObject.name);
    }
}

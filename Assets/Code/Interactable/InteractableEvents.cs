using UnityEngine;
using UnityEngine.Events;

public class InteractableEvents : MonoBehaviour
{
    public UnityEvent OnInteract;
}

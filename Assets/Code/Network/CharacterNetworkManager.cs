using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using BloodyPolyWar.Core.Singletons;
using Unity.Netcode;

public class CharacterNetworkManager : BloodyPolyWar.Core.Singletons.NetworkSingleton<CharacterNetworkManager>
{
    private NetworkVariable<int> playerInGame = new NetworkVariable<int>();

    public int PlayersInGame()
    {
        return playerInGame.Value;
    }

    private void Start()
    {
        NetworkManager.Singleton.OnClientConnectedCallback += (id) =>
        {
            if (IsServer)
            {
                playerInGame.Value++;
                Debug.Log($"{id} just connected");
            }
        };
        NetworkManager.Singleton.OnClientDisconnectCallback += (id) =>
        {
            if (IsServer)
            {
                playerInGame.Value--;
                Debug.Log($"{id} just disconnected");

            }
        };
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;

public class Look : MonoBehaviour
{
    // Sensivity x y
    // XRot
    // Cam
    public Camera camera;
    public float xSensivity = 30f;
    public float ySensivity = 30f;
    public float xTouchSensivity = 15f;
    public float yTouchSensivity = 15f;
    private float xRotation = 0f;

    public void ProcessLook(Vector2 input)
    {
        float mouseX = input.x;
        float mouseY = input.y;
        xRotation -= (mouseY * Time.deltaTime) * ySensivity;
        //Sag sola Bakma Acisi Sinirlama
        xRotation = Mathf.Clamp(xRotation, -45f, 45f);
        // cam transform
        camera.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
        transform.Rotate(Vector3.up * (mouseX * Time.deltaTime) * xSensivity);
    }
    public void ProcessLookTouch(Vector2 input)
    {
        float touchX = input.x;
        float touchY = input.y;
        xRotation -= (touchY * Time.deltaTime) * yTouchSensivity;
        Debug.Log("its contains");
        xRotation = Mathf.Clamp(xRotation, -45f, 45f);
            // cam transform
            camera.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);
            transform.Rotate(Vector3.up * (touchX * Time.deltaTime) * xTouchSensivity);

            //if(touchBound.x > (Screen.width / 2) && touchBound.y > (Screen.height / 2) - (Screen.height / 3))
       // {    Debug.Log("2");
            
      //  }
    }
}
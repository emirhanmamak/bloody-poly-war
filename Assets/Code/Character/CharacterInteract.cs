using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterInteract : MonoBehaviour
{
    private Camera _camera;
    [SerializeField] private float distance = 3f;
    [SerializeField] private LayerMask layerMask;
    private CharacterUI _chracterUI;
    private InputManager _inputManager;

    private void Awake()
    {
        _camera = GetComponent<Look>().camera;
        _chracterUI = GetComponent<CharacterUI>();
        _inputManager = GetComponent<InputManager>();
    }

    private void Update()
    {
        Interact();
    }

    public void Interact()
    {
        //Debug.Log("1");
        _chracterUI.UpdateText(string.Empty);
        Ray ray = new Ray(_camera.transform.position, _camera.transform.forward);
        Debug.DrawRay(ray.origin, ray.direction * distance, Color.magenta);
        RaycastHit _raycastHitInfo; //store of us hit raycast info
        if (Physics.Raycast(ray, out _raycastHitInfo, distance, layerMask))
        {
            //Debug.Log("2");

            if (_raycastHitInfo.collider.GetComponent<Interactable>() != null)
            {
                //ebug.Log("3");

                Interactable interactable = _raycastHitInfo.collider.GetComponent<Interactable>();
                _chracterUI.UpdateText((_raycastHitInfo.collider.GetComponent<Interactable>().promptMessage));
                if (_inputManager._onFootActions.Interact.triggered)
                {
                    interactable.BaseInteract();
                }
            }
        }
    }
    }

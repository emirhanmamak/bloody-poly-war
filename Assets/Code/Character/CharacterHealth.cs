using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Random = UnityEngine.Random;

public class CharacterHealth : MonoBehaviour
{
    [Header("HealthBar")]
    public float maxHealth = 100f;
    public float chipSpeed = 2f;
    public Image frontHealthBar;
    public Image backHealthBar;
    public TextMeshProUGUI healthText;
    private float health;
    private float lerpTimer = 0f;
    [Header("Damage Overlay")] public Image damageOverlay;
    public float duration;
    public float fadeSpeed;
    private float durationTimer;

    private void Start()
    {
        health = maxHealth;
    }

    private void Update()
    {
       // health = Mathf.Clamp(health, 0, maxHealth);
        //UpdateHealthUI();
       // DamageOverlay();
    }

    private void DamageOverlay()
    {
        if (damageOverlay.color.a > 0)
        {
            durationTimer += Time.deltaTime;
            if(health <= maxHealth / 4f) return;
            if (durationTimer > duration)
            {
                float tempAlpha = damageOverlay.color.a;
                tempAlpha -= Time.deltaTime * fadeSpeed;
                damageOverlay.color = new Color(damageOverlay.color.r, damageOverlay.color.g, damageOverlay.color.b,
                    tempAlpha);
            }
        }
    }

    public void UpdateHealthUI()
    {
        float fillFrontHealthBar = frontHealthBar.fillAmount;
        float fillBackHealthBar = backHealthBar.fillAmount;
        float healthBarFraction = health / maxHealth;

        if (fillBackHealthBar > healthBarFraction)
        {
            frontHealthBar.fillAmount = healthBarFraction;
            backHealthBar.color = Color.red;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipSpeed;
            percentComplete *= percentComplete;
            backHealthBar.fillAmount = Mathf.Lerp(fillBackHealthBar, healthBarFraction, percentComplete);
        }

        if (fillFrontHealthBar < healthBarFraction)
        {
            backHealthBar.color = Color.green;
            backHealthBar.fillAmount = healthBarFraction;
            lerpTimer += Time.deltaTime;
            float percentComplete = lerpTimer / chipSpeed;
            percentComplete *= percentComplete;
            frontHealthBar.fillAmount = Mathf.Lerp(fillFrontHealthBar, healthBarFraction, percentComplete);
        }

        UpdateHealthText();
    }

    public void TakeDamage(float damage)
    {
        health -= damage;
        durationTimer = 0f;
        lerpTimer = 0f;
        damageOverlay.color = new Color(damageOverlay.color.r, damageOverlay.color.g, damageOverlay.color.b, 1f);
    }

    public void RestoreDamage(float heal)
    {
        health += heal;
        lerpTimer = 0f;
    }

    private void UpdateHealthText()
    {
        healthText.text = health.ToString();
    }
}
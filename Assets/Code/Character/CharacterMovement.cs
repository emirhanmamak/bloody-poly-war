using System;
using Unity.Netcode;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(NetworkObject))]
public class CharacterMovement : NetworkBehaviour
{
   /* public enum CharacterStates
    {
        Idle,
        Walk,
        ReveseWalk,
        Sprint
    }

    public InputManager _InputManager;
    [Header("Movement")] 
    public bool isGrounded;
    public float gravitiy = -9.8f;
    
    // Movement Speed Varb.
    [SerializeField] private float movementSpeed = 0f;
    [SerializeField] private float walkSpeed = 1f;
    [SerializeField] private float reverseWalkSpeed = -1f;
    [SerializeField] private float rotationSpeed = 1.5f;
    [SerializeField] private float sprintSpeed = 8f;
    
    private CharacterController _characterController;
    private Vector3 _characterVelocity;
    private float _jumpHeight = 1f;

    private Animator _animator;
    
    private bool crouching;
    private bool lerpCrouch;
    private float crouchTimer;
    private bool issprinting;
    
    [Header("Network")] public bool network;

    [SerializeField] private NetworkVariable<Vector3> networkPositionDirection = new NetworkVariable<Vector3>();
    [SerializeField] private NetworkVariable<Vector3> networkRotationDirection = new NetworkVariable<Vector3>();

    [SerializeField] private NetworkVariable<Vector3> movmentPosition = new NetworkVariable<Vector3>();
    [SerializeField] private NetworkVariable<CharacterStates> networkCharacterState = new NetworkVariable<CharacterStates>();
    //[SerializeField] private NetworkVariable<float> leftRightPosition = new NetworkVariable<float>();
    
    [SerializeField]
    private Vector2 defaultInitalizePlanePosition = new Vector2(-4, 3);
    

    // client caches positions
    private Vector3 oldInputPosition;
    private Vector3 oldInputRotation;*/
   [SerializeField]
   private float walkSpeed = 3.5f;

   [SerializeField]
   private float runSpeedOffset = 2.0f;

   [SerializeField]
   private float rotationSpeed = 3.5f;

   [SerializeField]
   private Vector2 defaultInitialPositionOnPlane = new Vector2(-4, 4);

   [SerializeField]
   private NetworkVariable<Vector3> networkPositionDirection = new NetworkVariable<Vector3>();

   [SerializeField]
   private NetworkVariable<Vector3> networkRotationDirection = new NetworkVariable<Vector3>();

   [SerializeField]
   private NetworkVariable<PlayerState> networkPlayerState = new NetworkVariable<PlayerState>();

   private CharacterController characterController;

   // client caches positions
   private Vector3 oldInputPosition = Vector3.zero;
   private Vector3 oldInputRotation = Vector3.zero;
   private PlayerState oldPlayerState = PlayerState.Idle;

   private InputManager _inputManager;

   private Animator animator;
    private void Awake()
    {
        _inputManager = GetComponent<InputManager>();
        characterController = GetComponent<CharacterController>();
        animator = GetComponent<Animator>();
    }

    void Start()
    {
        if (IsClient && IsOwner)
        {
            transform.position = new Vector3(Random.Range(defaultInitialPositionOnPlane.x, defaultInitialPositionOnPlane.y), 0,
                Random.Range(defaultInitialPositionOnPlane.x, defaultInitialPositionOnPlane.y));
        }
    }

    void Update()
    {
        if (IsClient && IsOwner)
        {
            ClientInput();
        }

        ClientMoveAndRotate();
        ClientVisuals();
    }

    private void ClientInput()
    {
        // left & right rotation
        Vector3 inputRotation = new Vector3(0, _inputManager.GetRotation().normalized.x, 0);

        // forward & backward direction
        Vector3 direction = transform.TransformDirection(Vector3.forward);
        float forwardInput = _inputManager.GetPosition().y;
        Vector3 inputPosition = direction * forwardInput;

        // change animation states
        if (forwardInput == 0)
            UpdatePlayerStateServerRpc(PlayerState.Idle);
        else if (!ActiveRunningActionKey() && forwardInput > 0 && forwardInput <= 1)
            UpdatePlayerStateServerRpc(PlayerState.Walk);
        else if (ActiveRunningActionKey() && forwardInput > 0 && forwardInput <= 1)
        {
            inputPosition = direction * runSpeedOffset;
            UpdatePlayerStateServerRpc(PlayerState.Run);
        }
        else if (forwardInput < 0)
            UpdatePlayerStateServerRpc(PlayerState.ReverseWalk);

        // let server know about position and rotation client changes
        if (oldInputPosition != inputPosition ||
            oldInputRotation != inputRotation)
        {
            oldInputPosition = inputPosition;
            UpdateClientPositionAndRotationServerRpc(inputPosition * walkSpeed, inputRotation * rotationSpeed);
        }
        
        
        
        
        
        
        
        
        /*
         * Character Position and Rotation Input
         */
        /*Vector3 moveDirection = Vector3.zero;
        Vector2 input = _InputManager.GetPosition();
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        _characterController.Move(transform.TransformDirection((moveDirection) * movementSpeed * Time.deltaTime));*/

       // Vector3 forwardInput = new Vector3(0, _InputManager.GetPosition().normalized.x, _InputManager.GetPosition().normalized.y);
       /*Vector3 moveDirection = Vector3.zero;
       moveDirection.x = _InputManager.GetPosition().x;
       moveDirection.z = _InputManager.GetPosition().y;
       _characterController.Move(transform.TransformDirection((moveDirection) * movementSpeed * Time.deltaTime));
       UpdateClientPositionAndRotationServerRpc((moveDirection) * movementSpeed * Time.deltaTime);
       
       /* Vector3 direction = transform.TransformDirection(Vector3.forward);
        float forwardInput = _InputManager.GetPosition().normalized.y;
        Vector3 inputPosition = direction * forwardInput;
       

        if (oldInputPosition != inputPosition)
        {
            _characterController.Move(inputPosition * Time.deltaTime * movementSpeed);
            UpdateClientPositionAndRotationServerRpc(inputPosition * Time.deltaTime * movementSpeed);
        }*/
        
        /*
         * Character States Changes base on Input
         */
        /*
        if (_characterController.velocity.y > walkSpeed && _characterController.velocity.y > 0)
        {
            UpdateCharacterStateServerRpc(CharacterStates.Sprint);
        }
        else if (_characterController.velocity.y > 0)
        {
            UpdateCharacterStateServerRpc(CharacterStates.Walk);
        }
        else if (_characterController.velocity.y < 0)
        {
            UpdateCharacterStateServerRpc(CharacterStates.ReveseWalk);
        }
        else
        {
            UpdateCharacterStateServerRpc(CharacterStates.Idle);
        }*/
    }
    private void ClientMoveAndRotate()
    {
        if (networkPositionDirection.Value != Vector3.zero)
        {
            characterController.SimpleMove(networkPositionDirection.Value);
        }
        if (networkRotationDirection.Value != Vector3.zero)
        {
            transform.Rotate(networkRotationDirection.Value, Space.World);
        }
    }
    private void ClientVisuals()
    {
        if (oldPlayerState != networkPlayerState.Value)
        {
            oldPlayerState = networkPlayerState.Value;
            animator.SetTrigger($"{networkPlayerState.Value}");
        }
    }
    private void Gravity()
    {
      /*  if (isGrounded && _characterVelocity.y < 0f)
        {
            _characterVelocity.y = -2f;
        }

        _characterVelocity.y += gravitiy * Time.deltaTime;
//        _characterController.Move(_characterVelocity * Time.deltaTime);*/
    }

    private static bool ActiveRunningActionKey()
    {
        return Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift);
    }

    [ServerRpc]
    public void UpdateClientPositionAndRotationServerRpc(Vector3 newPosition, Vector3 newRotation)
    {
        networkPositionDirection.Value = newPosition;
        networkRotationDirection.Value = newRotation;
    }

    [ServerRpc]
    public void UpdatePlayerStateServerRpc(PlayerState state)
    {
        networkPlayerState.Value = state;
    }
}
/*public class Movement : MonoBehaviour
{    
    public bool isGrounded;
    public float gravitiy = -9.8f;
    
    // Movement Speed Varb.
    [SerializeField] private float movementSpeed = 0f;
    [SerializeField] private float walkSpeed = 5f;
    [SerializeField] private float sprintSpeed = 8f;
    
    private CharacterController _characterController;
    private Vector3 _characterVelocity;
    private float _jumpHeight = 1f;

    private bool crouching;
    private bool lerpCrouch;
    private float crouchTimer;
    private bool issprinting;

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
    }

    private void Start()
    {
        movementSpeed = walkSpeed;
    }

    private void Update()
    {
        isGrounded = _characterController.isGrounded;
        if (lerpCrouch)
        {
            crouchTimer += Time.deltaTime;
            float p = crouchTimer / 1;
            p *= p;
            if (crouching)
            {
                _characterController.height = Mathf.Lerp(_characterController.height, 1, p);
            }
            else
            {
                _characterController.height = Mathf.Lerp(_characterController.height, 2, p);
            }

            if (p >= 1)
            {
                lerpCrouch = false;
                crouchTimer = 0f;
            }
        }

    }
    public void CharacterMovement(Vector2 input)
    {
        Vector3 moveDirection = Vector3.zero;
        moveDirection.x = input.x;
        moveDirection.z = input.y;
        _characterController.Move(transform.TransformDirection((moveDirection) * movementSpeed * Time.deltaTime));
        
        // GRAVITY
        Gravity();
    }

    private void Gravity()
    {
        if (isGrounded && _characterVelocity.y < 0f)
        {
            _characterVelocity.y = -2f;
        }

        _characterVelocity.y += gravitiy * Time.deltaTime;
        _characterController.Move(_characterVelocity * Time.deltaTime);
    }

    public void Jump()
    {
        if (isGrounded) 
        {
            _characterVelocity.y = Mathf.Sqrt(_jumpHeight * -3f * gravitiy);
        }
        // Debug.Log("dfsdfsd");
    }

    public void Crouch()
    {
        crouching = !crouching;
        crouchTimer = 0;
        lerpCrouch = true;
    }

    public void Sprint()
    {
        issprinting = !issprinting;
        if (issprinting)
        {
            movementSpeed = sprintSpeed;
        }
        else
        {
            movementSpeed = walkSpeed;
        }
    }
}*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputManager : MonoBehaviour
{
    public CharacterInput _characterInput;
    public CharacterInput.OnFootActions _onFootActions;
    private CharacterMovement _movement;
    private Look _look;

    //public RectTransform lookTouchPanel;
    //private Interactable _interactable;

    int fingerCount;
    //Character Variables



    private void Awake()
    {
        _characterInput = new CharacterInput();
        _onFootActions = _characterInput.OnFoot;
        _movement = GetComponent<CharacterMovement>();
        _look = GetComponent<Look>();
        //_interactable = GetComponent<Interactable>();
       // _onFootActions.Jump.performed += ctx => _movement.Jump();
       // _onFootActions.Sprint.performed += ctx => _movement.Sprint();
       // _onFootActions.Crouch.performed += ctx => _movement.Crouch();
    }

    private void FixedUpdate()
    {
       // _movement.CharacterMove(_onFootActions.Movement.ReadValue<Vector2>());
        //_movement.CharacterMovement((_onFootActions.LeftStickGP.ReadValue<Vector2>()));
        //_movement.CharacterMovement((_onFootActions.MobileJoyStick.ReadValue<Vector2>()));
    }
    
    private void LateUpdate()
    {
      /*  if (_onFootActions.Touch.ReadValue<Vector2>().Equals(lookTouchPanel.transform))
        {
            Debug.Log("ch(_onFootActions.Touch.ReadValue<");
            _look.ProcessLookTouch(_onFootActions.Touch.ReadValue<Vector2>());
        }
        else
        {
            _look.ProcessLook(_onFootActions.Look.ReadValue<Vector2>());
        }*/

    }

    private void OnEnable()
    {
        _onFootActions.Enable(); 
    }

    private void OnDisable()
    {
        _onFootActions.Disable();
    }

    public bool GetIsJump()
    {
        return _characterInput.OnFoot.Jump.triggered;
    }

    public Vector2 GetTouchDelta()
    {
        return _characterInput.UI.Look.ReadValue<Vector2>();
    }

    public Vector2 GetRotation()
    {
        return _onFootActions.Look.ReadValue<Vector2>();
    }
    public Vector2 GetPosition()
    {
        return _onFootActions.Movement.ReadValue<Vector2>();
    }
    
}

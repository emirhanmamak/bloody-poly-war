using System;
using UnityEngine;
using TMPro;
using Unity.Netcode;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] private Camera camera;
    [SerializeField] private Button startHost;
    [SerializeField] private Button startServer;
    [SerializeField] private Button startClient;
    [SerializeField] private Button executePhysics;
    [SerializeField] private TextMeshProUGUI playerInGameText;
    private bool hasServerStarted;

    private void Awake()
    {
        Cursor.visible = true;
    }

    private void Start()
    {
        startHost.onClick.AddListener(() =>
        {
            if (NetworkManager.Singleton.StartHost())
            {
                Debug.Log("HOST STARTED");
                camera.gameObject.SetActive(false);
            }
        });
        startServer.onClick.AddListener(() =>
        {
            if (NetworkManager.Singleton.StartServer())
            {
                Debug.Log("SERVER STARTED");
                camera.gameObject.SetActive(false);

                // NetworkManager.Singleton.
            }
        });
        startClient.onClick.AddListener(() =>
        {
            if (NetworkManager.Singleton.StartClient())
            {
                camera.gameObject.SetActive(false);

                Debug.Log("CLIENT STARTED");
            }
        });
        NetworkManager.Singleton.OnServerStarted += () =>
        {
            hasServerStarted = true;
        };
        executePhysics.onClick.AddListener(() =>
        {
            if(!hasServerStarted) return;
            SpawnerControl.Instance.SpawnObjects();
            camera.gameObject.SetActive(true);
        });
    }

    private void Update()
    {
        playerInGameText.text = $"Players in game: {CharacterNetworkManager.Instance.PlayersInGame()}";
    }
}
